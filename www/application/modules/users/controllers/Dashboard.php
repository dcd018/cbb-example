<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
    }

    public function index()
    {
        if (!$this->session->email) {
            redirect('/users/login');
        }

        $users = $this->user->findAll();
        $this->load->view('header');
        $this->load->view('dashboard', ['users' => $users]);
        $this->load->view('footer');
    }
}