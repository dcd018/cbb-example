<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->config('../config/config.php');
        $this->load->model('user_model', 'user');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->library('facebook');
    }

    public function register()
    {
        $post = $this->_derivePostFromAuthCode();
        if ($post) {
            $post = $this->security->xss_clean($post);
            $this->_validateRegistration($post);
        }
        else {
            $post = $this->security->xss_clean($this->input->post(null, true));
            $this->_register($post);
        }   
    }

    public function verify()
    {
        $segment = $this->_base64_decode($this->uri->segment(3));
        $token = $this->security->xss_clean($segment);

        try {
            $user = $this->user->hasValidToken($token);
            $this->user->update((array) $user);
            $this->session->set_flashdata('info', 'Your account has been verified.');
        } catch (Exception $e) {
            $this->session->set_flashdata('warning', $e->getMessage());
        }
        redirect('/users/login');
    }

    public function login()
    {
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');
        
        if ($this->form_validation->run() == false) {
            $this->load->view('header');
            $this->load->view('login');
            $this->load->view('footer');
        }
        else {
            $post = $this->security->xss_clean($this->input->post(null, true));
            try {
                $user = $this->user->validateLogin($post);
                $userdata = $this->config->item('sess_userdata');
                foreach ($user as $key => $val) {
                    if (array_search($key, $userdata) !== false) {
                        $this->session->{$key} = $val;
                    }
                }
                redirect('/users/dashboard');
            } catch (Exception $e) {
                $this->session->set_flashdata('warning', $e->getMessage());
                redirect('/users/login');
            }
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('/users/login');
    }

    private function _register($post)
    {
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');

        if ($this->form_validation->run() == false) {
            $data = [
                'fb_login_link' => $this->facebook->getLoginLink()
            ];
            $this->load->view('header');
            $this->load->view('register', $data);
            $this->load->view('footer');
        }
        else {
            unset($post['confirm_password']);
            $this->_validateRegistration($post);
        }
    }

    private function _derivePostFromAuthCode()
    {
        if (!$this->input->get('code')) {
            return;
        }
        try {
            $post = $this->facebook->getProfile();
            $post['facebook_id'] = $post['id'];
            $post['password'] = bin2hex(openssl_random_pseudo_bytes(4));
            unset($post['id']);
            return $post;
        } catch (Exception $e) {
            $this->session->set_flashdata('warning', $e->getMessage());
            redirect('/users/register');
        }
    }

    private function _validateRegistration($post)
    {
        if ($this->user->exists($post['email'])) {
            $this->session->set_flashdata('warning', 'User already exists, please try signing in.');
        }
        else {
            $id = $this->user->create($post);
            $this->_sendVerificationEmail($id);
            $this->session->set_flashdata('info', 'To verify your account, please check your email.');
        }
        redirect('/users/login');
    }

    private function _sendVerificationEmail($userId)
    {
        try {
            $user = $this->user->find($userId);
        } catch (Exception $e) {
            $this->session->set_flashdata('warning', $e->getMessage());
            redirect('/users/register');
        }

        $token = $this->user->createToken($user->id);
        $url = sprintf('%susers/verify/%s', site_url(), $this->_base64_encode($token));
        
        $body = 'Welcome to CBB Example!';
        if ($user->facebook_id) {
            $body .= sprintf(
                '<p>Thank you for registering with Facebook, your temporary password is <strong>%s</strong></p>', 
                $this->encryption->decrypt($user->password)
            );
        }
        $body .= sprintf(
            '<p><a href="%s">Please verify your account by visiting this link.</a></p>', 
            $url
        );

        $this->email->from($this->config->item('smtp_user'), 'CBB');
        $this->email->to($user->email);
        $this->email->subject('Please verify your account.');
        $this->email->message($body);
        $this->email->send();
    }

    private function _base64_encode($data) { 
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
    }

    private function _base64_decode($data) { 
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
    }
}