<?php
class User_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('encryption');
        $this->tokenExpiration = $this->config->item('token_expiration');
    }
    
    public function create($post)
    {
        $post['password'] = !$post['password'] ?: $this->encryption->encrypt($post['password']);
        $this->db->insert('users', $post);
        return $this->db->insert_id();
    }

    public function exists($email)
    {
        $q = $this->db->get_where('users', ['email' => $email]);
        return $q->num_rows() > 0;
    }

    public function createToken($userId)
    {
        $token = $this->_genUUID();
        $this->db->insert('tokens', [
            'token' => $token,
            'user_id' => $userId
        ]);
        return sprintf('%s%s', $token, $userId);
    }

    public function hasValidToken($str)
    {
        $token = substr($str, 0, 36);
        $id = substr($str, 36);

        $q = $this->db->get_where('tokens', [
            'token' => $token,
            'user_id' => $id
        ]);

        if (!$q->num_rows()) {
            throw new Exception('Unable to verify your account.');
        }

        $row = $q->row();
        $created = strtotime($row->created);
        $now = strtotime(date('Y-m-d H:i:s'));
        
        if (($now - $created) > $this->tokenExpiration) {
            throw new Exception('Token has expired, please try again later.');
        }
        
        return $this->find($id);
    }

    public function find($id)
    {
        $q = $this->db->get_where('users', ['id' => $id]);
        if (!$q->num_rows()) {
            throw new Exception('User not found.');
        }
        return $q->row();
    }

    public function findByEmail($email)
    {
        $q = $this->db->get_where('users', ['email' => $email]);
        if (!$q->num_rows()) {
            throw new Exception('User not found.');
        }
        return $q->row();
    }

    public function findAll() 
    {
        $this->db->select('first_name, last_name, email, last_seen');
        $q = $this->db->get('users');
        return $q->result();
    }

    public function update($post)
    {
        list('id' => $id) = $post;
        
        $this->db->where('id', $id);
        $this->db->update('users', [
            'last_seen' => date('Y-m-d H:i:s'),
            'is_verified' => true
        ]);

        if (!$this->db->affected_rows()) {
            throw new Exception('Unable to update user.');
        }

        return $this->find($id);
    }

    public function validateLogin($post)
    {
        list('email' => $email, 'password' => $password) = $post;
        $user = $this->findByEmail($email);

        if (!$user->is_verified) {
            throw new Exception('Account has not been verified, please check your email.');
        }

        if ($password !== $this->encryption->decrypt($user->password)) {
            throw new Exception('Invalid password.');
        }

        $this->db->where('id', $user->id);
        $this->db->update('users', ['last_seen' => date('Y-m-d H:i:s')]);

        unset($user->password);
        return $user;
    }

    /**
     * https://www.php.net/manual/en/function.uniqid.php#94959
     */
    private function _genUUID()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
    
            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),
    
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,
    
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,
    
            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
}