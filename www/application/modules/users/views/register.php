<?php echo form_open('users/register', ['class' => 'form-cbb']);?>
  <h1 class="h3 mb-3 font-weight-normal">Create your account</h1>
  <label for="inputFirstName" class="sr-only">First name</label>
  <input name="first_name" type="text" id="inputFirstName" class="form-control form-control-start" placeholder="First name" required autofocus>

  <label for="inputLastName" class="sr-only">Last name</label>
  <input name="last_name" type="text" id="inputLastName" class="form-control form-control-nth" placeholder="Last name" required autofocus>

  <label for="inputEmail" class="sr-only">Email address</label>
  <input name="email" type="email" id="inputEmail" class="form-control form-control-nth" placeholder="Email address" required autofocus>
  
  <label for="inputPassword" class="sr-only">Password</label>
  <input name="password" type="password" id="inputPassword" class="form-control form-control-nth" placeholder="Password" required>

  <label for="inputConfirmPassword" class="sr-only">Confirm password</label>
  <input name="confirm_password" type="password" id="inputConfirmPassword" class="form-control form-control-end" placeholder="Confirm password" required>
  
  <button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
  
  <div class="row">
    <div class="col-sm form-links">
      <ul>
        <li>
          <a href="<?php echo $fb_login_link;?>" class="btn btn-lg btn-social btn-facebook btn-block">
            <i class="fab fa-facebook-f"></i>Sign up with Facebook
          </a>
        </li>
        <li>
          <a href="<?=site_url('users/login')?>">Sign in</a>
        </li>
      </ul>
    </div>
  </div>
<?php echo form_close();?>