<?php echo form_open('users/login', ['class' => 'form-cbb']);?>
  <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
  
  <label for="inputEmail" class="sr-only">Email address</label>
  <input name="email" type="email" id="inputEmail" class="form-control form-control-start" placeholder="Email address" required autofocus>
  
  <label for="inputPassword" class="sr-only">Password</label>
  <input name="password" type="password" id="inputPassword" class="form-control form-control-end" placeholder="Password" required>
  
  <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
  
  <div class="row">
    <div class="col-sm form-links">
      <ul>
        <li>
          <a href="<?php echo site_url('users/register');?>">Sign up</a>
        </li>
      </ul>
    </div>
  </div>
<?php echo form_close();?>