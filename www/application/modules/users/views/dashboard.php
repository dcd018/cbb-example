<div class="card card-cbb">
  <div class="card-body">
    <table class="table table-borderless">
      <thead>
        <tr>
          <th scope="col">First name</th>
          <th scope="col">Last name</th>
          <th scope="col">Email</th>
          <th scope="col">Last seen</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($users as $user): ?>
          <tr>
            <td><?php echo $user->first_name;?></td>
            <td><?php echo $user->last_name;?></td>
            <td><?php echo $user->email;?></td>
            <td><?php echo date('F j, Y, g:i a', strtotime($user->last_seen));?></td>
          </tr>
        <?php endforeach;?>
      </tbody>
    </table>
  </div>
</div>