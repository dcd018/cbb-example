<?php

class Facebook {

    protected $CI;

    protected $fb;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->config('../config/config.php');
        
        $this->fb = new Facebook\Facebook([
            'app_id' => $this->CI->config->item('fb_app_id'),
            'app_secret' => $this->CI->config->item('fb_app_secret'),
            'default_graph_version' => 'v3.3'
        ]);
    }
    
    public function getLoginLink()
    {
        $helper = $this->fb->getRedirectLoginHelper();
        $permissions = ['email'];
        $loginUrl = $helper->getLoginUrl(sprintf('%susers/register', site_url()), $permissions);
        return htmlspecialchars($loginUrl);
    }

    public function getAccessToken()
    {
        $helper = $this->fb->getRedirectLoginHelper();

        $accessToken = $helper->getAccessToken();
        
        if (!isset($accessToken)) {
            $err = ($msg = $helper->getError() && $msg)
                ? $msg
                : 'Bad Request';
            
            throw new Exception($err);
        }
        
        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $this->fb->getOAuth2Client();
        
        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);

        // Validation
        $tokenMetadata->validateAppId($this->CI->config->item('fb_app_id'));
        $tokenMetadata->validateExpiration();

        // Exchanges a short-lived access token for a long-lived one
        if (!$accessToken->isLongLived()) {
            $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
        }

        return $accessToken;
    }

    public function getProfile()
    {
        $accessToken = $this->getAccessToken();

        // Returns a `Facebook\FacebookResponse` object
        $response = $this->fb->get('/me?fields=id,first_name,last_name,email', $accessToken->getValue());
        return $response->getGraphUser()->asArray();
    }
}