<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Session Userdata
|--------------------------------------------------------------------------
|
| For use with mapping $_SESSION
|
*/
$config['sess_userdata'] = ['first_name', 'last_name', 'email'];

/*
|--------------------------------------------------------------------------
| Facebook SDK Credentials
|--------------------------------------------------------------------------
|
| For use with the Facebook SDK
|
*/
$config['fb_app_id'] = '657971191330941';
$config['fb_app_secret'] = 'c19b01f0fe212ddcc456e75da612bc98';