<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>CBB Example</title>

    <link rel="canonical" href="https://www.caribbeanbluebook.com/">

    <!-- Bootstrap core CSS -->
    <?=link_tag('assets/css/bootstrap.min.css');?>

    <!-- Custom styles for this template -->
    <?=link_tag('assets/css/style.css');?>

    <script src="https://kit.fontawesome.com/35486f3a32.js"></script>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark navbar-cbb">
      <a class="navbar-brand" href="#">CBB Example</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
        <ul class="navbar-nav">
          <?php if (empty($this->session->email)):?>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo site_url('users/login');?>">Sign in</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo site_url('users/register');?>">Sign up</a>
            </li>
          <?php else:?>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo site_url('users/logout');?>">Sign out</a>
            </li>
          <?php endif;?>
        </ul>
      </div>
    </nav>
    <div class="content text-center">
      <?php if (!empty($this->session->flashdata('info'))):?>
        <div class="alert alert-success" role="alert">
          <?php echo $this->session->flashdata('info');?>
        </div>
      <?php endif;?>
      <?php if (!empty($this->session->flashdata('warning'))):?>
        <div class="alert alert-danger" role="alert">
          <?php echo $this->session->flashdata('warning');?>
        </div>
      <?php endif;?>
      <?php $err = validation_errors(); if (!empty($err)):?>
        <div class="alert alert-danger" role="alert">
          <?php echo $err;?>
        </div>
      <?php endif;?>