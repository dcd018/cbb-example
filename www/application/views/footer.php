    </div>
    <footer class="footer mt-auto py-3">
      <div class="container text-center">
        <span class="text-muted">©2019. Caribbean BlueBook LLC (Example by NDA). All Rights Reserved.</span>
      </div>
    </footer>

    <!-- Include JS -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
  </body>
</html>