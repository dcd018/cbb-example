# Example CodeIgniter Application
A login system with Facebook registration capabilities using HMVC modules 

### Docker
This package uses Docker Compose by default to build and deploy containers that are configured to communicate with each other. 

```sh
git clone git@bitbucket.org:dcd018/cbb-example.git
cd cbb-example
docker-compose up -d
```
This will create Apache2, MySQL, phpMyAdmin images, pull in necessary dependencies and build the containers.
Once done, verify the deployment by navigating to your server address in your preferred browser.

```sh
# Example application
http://localhost:3000
# phpMyAdmin
http://localhost:8080
```
### Local LAMP Stack
If Docker Compose isn't configured within your local environment or deploying directly on a host machine is preferred, ensure you have Apache 2.4, MySQL 5.3, PHP 7.2 and the lates version of Composer installed. Following the offical CodeIgniter documentation should be sufficient in deploying the application from the `/www` directory. CodeIgniter configuration options relavent here are those specific to your local database configuration and `base_url`. Once the database is set up, table schemas used by the application can be imported via dump located in `/bin/mysql/dump/cbb-example.sql`.

Once configured, ensure dependencies are installed
```sh
cd cbb-example/www/application
composer install
```
### Notes

###### SMTP
SMTP is configued to use a Gmail account created specifically for testing purposes and accounts registerd with non-Gmail accounts may not receive verification emails. To ensure registration functions as expected, using a Gmail account or environment specific SMTP settings is reccomended. The Gmail account used by the SMTP configuration located in `/www/application/config/email.php` may also be used granted usage limits are not reached.

###### Registration with Facebook
If you've opted to deploy via local LAMP stack, ensure you do so without an alias as Facebook does not allow non SSL OAuth2 redirect URLs and localhost is whitelisted by default.




